<?php

Route::get('/login', 'LoginController@showLogin')->name('show-login');
Route::post('/login', 'LoginController@actionLogin')->name('action-login');

Route::get('/register', 'RegisterController@showRegister')->name('show-register');
Route::post('/register', 'RegisterController@actionRegister')->name('action-register');

Route::get('/forgot-password', 'ForgotPasswordController@showForgotPassword')->name('show-forgot-password');
Route::post('/forgot-password', 'ForgotPasswordController@actionForgotPassword')->name('action-forgot-password');


