<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function actionLogin($loginData)
    {
        $hashPassword = $this->user->whereUsername($loginData['username'])->value('password');
        $checkPassword = Hash::check($loginData['password'], $hashPassword);
        if ($checkPassword) {
            return 'oke';
        }
        return '';
    }
}