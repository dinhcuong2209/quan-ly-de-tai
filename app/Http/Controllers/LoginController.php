<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use App\Services\AuthService;


class LoginController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function showLogin()
    {
        return view('index');
    }

    public function actionLogin(LoginFormRequest $request)
    {
        $loginData = $this->getLoginData($request);
        $responseData = $this->authService->actionLogin($loginData);
        dd($responseData);
    }

    protected function getLoginData(LoginFormRequest $request)
    {
        return [
            'username' => $request['username'],
            'password' => $request['password'],
        ];
    }
}
